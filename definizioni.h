//
// Created by Alessandro on 18/03/2020.
//

#ifndef VESTITI_DEFINIZIONI_H
#define VESTITI_DEFINIZIONI_H
//
// Created by Alessandro on 18/03/2020.
//
#include "funzioni.h"
#include <stdio.h>

int leggi(char nomefile [100], struct vestiti *ves) {
    FILE* fp=fopen(nomefile,"r");
    int i =0;
    ves->num=0;
    if (fp) {
        while(!feof(fp)){
            fscanf(fp, "%d %d %d %f", &ves->vest[i].tipo, &ves->vest[i].taglia, &ves->vest[i].quantita, &ves->vest[i].prezzo);
            printf("%d %d %d %f \n", ves->vest[i].tipo, ves->vest[i].taglia, ves->vest[i].quantita, ves->vest[i].prezzo);
            i++;
            ves->num++;
        }
        fclose(fp);
        return 1;
    }else{
        return 0;
    }

}
void estrai(int taglia, struct vestiti ves1, struct vestiti *ves2){
    ves2->num=0;
    for (int i = 0; i < ves1.num ; ++i) {
        if(ves1.vest[i].taglia==taglia){
            ves2->vest[ves2->num].tipo=ves1.vest[i].tipo;
            ves2->vest[ves2->num].taglia=ves1.vest[i].taglia;
            ves2->vest[ves2->num].quantita=ves1.vest[i].quantita;
            ves2->vest[ves2->num].prezzo=ves1.vest[i].prezzo;
            ves2->num++;
        }
    }
}

int quant(struct vestiti ves){
    int totvestiti=0;
    for (int i = 0; i <ves.num ; ++i) {
        totvestiti+=ves.vest[i].quantita;
    }

    return totvestiti;
}

float prezzo(struct vestiti ves){
    float totprezzo=0;
    for (int i = 0; i <ves.num ; ++i) {
        totprezzo+=ves.vest[i].prezzo * (float)ves.vest[i].quantita;
    }

    return totprezzo;
}

int scrivi(char nomefile[], struct vestiti ves){
    FILE* fp=fopen(nomefile,"w");
    if (fp) {
        for (int i = 0; i <ves.num ; ++i) {

            if (ves.vest[i].taglia>=42 && ves.vest[i].taglia<=54 ) {
                fprintf(fp, "%d %d %d %f\n", ves.vest[i].tipo, ves.vest[i].taglia, ves.vest[i].quantita,ves.vest[i].prezzo);
            }

        }
        fclose(fp);
        return 1;
    }else{
        return 0;
    }
}
#endif //VESTITI_DEFINIZIONI_H
