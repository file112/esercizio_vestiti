#include <stdio.h>
#include "definizioni.h"


int main() {
    struct vestiti ves;
    struct vestiti ves2;
    int a,a1;
    int totvestiti;
    float totprezzo;
    int taglia=22;
    a=leggi("Vestiti.txt",&ves);
    if(a){
        estrai(taglia,ves,&ves2);
        printf("Vestiti taglia %d:\n", taglia);
        for (int i = 0; i <ves2.num ; ++i) {
            printf("%d %d %d %f \n", ves2.vest[i].tipo, ves2.vest[i].taglia, ves2.vest[i].quantita, ves2.vest[i].prezzo);
        }
        totvestiti=quant(ves);
        printf("In totale ci sono %d vestiti\n",totvestiti);
        totprezzo=prezzo(ves);
        printf("Il prezzo totale dei vestiti e': %f\n",totprezzo);
        a1=scrivi("Totali.txt",ves);
        if (a1){
            printf("File creato correttamente\n");
        } else{
            printf("Errore nella creazione del file\n");
        }
    } else{
        printf("Errore nell'apertura del file\n");
    }




    return 0;
}
