//
// Created by Alessandro on 18/03/2020.
//

#ifndef VESTITI_FUNZIONI_H
#define VESTITI_FUNZIONI_H
struct  vestito{
    int tipo;
    int taglia;
    int quantita;
    float prezzo;
};

struct vestiti{
    struct vestito vest[20];
    int num;
};

int leggi(char nomefile[],struct vestiti *ves);

void estrai(int taglia, struct vestiti ves1, struct vestiti *ves2);

int quant(struct vestiti ves);

float prezzo(struct vestiti ves);

int scrivi(char nomefile[], struct vestiti ves);


#endif //VESTITI_FUNZIONI_H
